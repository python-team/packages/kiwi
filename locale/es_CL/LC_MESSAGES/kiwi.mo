��    @        Y         �  %   �  '   �  &   �     �  (         :     [     _     g     �     �     �  
   �     �     �     �     �     �  /        @     E     L     R  )   X  %   �     �  
   �  	   �     �     �     �  	   �     �     �     �     �            5     9   S     �  :   �  $   �     	     
	     	     	     	  	   #	  #   -	  ?   Q	  @   �	     �	     �	     �	     �	     �	     �	     �	     
     
     

     
  J  
  %   ]  $   �  (   �     �  +   �  !     
   <     G     P      p  &   �     �     �     �     �     �     �     �  #        7  	   <     F     O  /   V  +   �     �     �     �     �     �     �     �  
   �     
  
             3     J  3   Q  9   �     �  :   �  (        =     B     H     L     S     [  /   `  :   �  6   �  	                       !     $     -     9     <     ?     B        6                      *      ?       /      !       9   :         #   +                              8                     -       "   2       	       
   &             <   3          ;       4                  ,       '         @      (   $   .          )   =   0           7   1          %                 >       5    %s can not be converted to a currency %s could not be converted to an integer '%s' can not be converted to a boolean '%s' is not a valid object '%s' is not a valid value for this field A file named "%s" already exists Any Boolean Could not load image: %s Could not open file "%s" Could not select folder "%s" Currency Custom day Custom interval Date Date and Time Decimal Do you want to remove %s ? Do you wish to replace it with the current one? Enum Finish Float From: Inproperly placed thousand separators: %r Inproperly placed thousands separator Integer Last month Last week Long Object Open Password: Replace Save Search: Select folder Show more _details String The file "%s" could not be opened. Permission denied. The folder "%s" could not be selected. Permission denied. This field is mandatory This field requires a date of the format "%s" and not "%s" This field requires a number, not %r Time To: Today Total: Unicode Yesterday You cannot enter a year before 1900 You have a thousand separator to the right of the decimal point You have more than one decimal point ("%s")  in your number "%s" _Cancel _Select _Today dd hh hh:mm:ss hh:mm:ss LL mm ss yy yyyy Project-Id-Version: 1.9.20
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-05-28 17:38-0300
PO-Revision-Date: 2008-03-06 01:07-0300
Last-Translator: Gabriel Felipe Cornejo Salas <gnomeusr@gmail.com>
Language-Team: Español/Chile <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 %s no se puede convertir a una moneda %s no se pudo convertir en un entero '%s' no se pudo convertir en un booleano '%s' no es un objeto válido '%s' no es un valor válido para este campo Ya existe un archivo llamado "%s" Cualquiera Booleano No se pudo cargar la imagen: %s No se pudo abrir el archivo "%s" No se pudo seleccionar la carpeta "%s" Moneda Día de encargo Intervalo de encargo Fecha Fecha y Hora Decimal ¿Desea borrar %s ? ¿Desea reemplazarlo con el actual? Enum Finalizar Flotante Desde: Separador de miles incorrectamente colocado: %r Separador de miles incorrectamente colocado Entero El mes pasado La semana pasada Entero largo Objeto Abrir Contraseña: Reemplazar Guardar Búsqueda: Seleccione la carpeta Muestra más _detalles Cadena El archivo "%s" no se pudo abrir. Permiso denegado. La carpeta "%s" no se pudo seleccionar. Permiso denegado. Este campo es obligatorio Este campo requiere una fecha en el formato "%s" y no "%s" Este campo requiere un número, no un %r Hora Para: Hoy Total: Unicode Ayer No se puede introducir un año anterior al 1900 Tiene un separador de miles a la derecha del punto decimal Hay más de un punto decimal ("%s") en el número "%s" _Cancelar _Seleccionar _Hoy dd hh hh:mm:ss hh:mm:ss LL mm ss aa aaaa 